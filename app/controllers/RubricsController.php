<?php

namespace app\controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class RubricsController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'app\controllers\RubricsController::find');

        return $controllers;
    }

    public function find(Request $request, Application $app)
    {

    }
}